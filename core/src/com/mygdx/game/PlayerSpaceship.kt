package com.mygdx.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Polygon
import com.mygdx.handlers.InGameHandler
import com.mygdx.values.Constants


class PlayerSpaceship : Spaceship{

    override var health: Int = 1
    override var vertices = floatArrayOf(0.0f+Constants.SPACESHIP_INITIAL_X_POS, 9.0f+Constants.SPACESHIP_INITIAL_Y_POS, 48.0f+Constants.SPACESHIP_INITIAL_X_POS, 75.0f+Constants.SPACESHIP_INITIAL_Y_POS, 98.0f+Constants.SPACESHIP_INITIAL_X_POS, 9.0f+Constants.SPACESHIP_INITIAL_Y_POS)
    override var poly = Polygon()
    override var moveRight: Boolean = false
    override var moveLeft: Boolean = false
    private var shots = listOf< Pair<Float, Float> >()
    private var shotSound: Sound


    constructor(game: SpaceInvadersGame) : super(){
        this.setPosition(Constants.SPACESHIP_INITIAL_X_POS, Constants.SPACESHIP_INITIAL_Y_POS)
        this.setSize(Constants.SPACESHIP_WIDTH, Constants.SPACESHIP_HEIGHT)
        this.addListener(InGameHandler(this, game))
        this.shotSound = Gdx.audio.newSound(Gdx.files.internal(Constants.SHOT_SOUND))
        this.updateSpaceshipPoly()
    }

    override fun moveRight(deltaX: Float){
        if(this.moveRight && (this.x + deltaX + Constants.SPACESHIP_WIDTH <= Constants.BASE_GAME_WIDTH)) {
            this.setPosition(this.x + deltaX, this.y)
            //ATTACK ITTER
            for (i in 0 until this.vertices.size) {
                if(i%2 == 0) {
                    this.vertices[i] = this.vertices[i] + deltaX
                    this.updateSpaceshipPoly()
                }
            }

        }
    }

    override fun moveLeft(deltaX: Float){
        if(this.moveLeft && (this.x +deltaX >= 0)) {
            this.setPosition(this.x +deltaX, this.getY())
            //ATTACK ITTER
            for (i in 0 until this.vertices.size) {
                if(i%2 == 0) {
                    this.vertices[i] = this.vertices[i]+deltaX
                    this.updateSpaceshipPoly()
                }
            }
        }
    }


    /*
    //VERSAO SEM CURRYING
    fun moveLeftAndRight(deltaX: Float, sumOrSubtract: (Float, Float) -> Float){
        if((sumOrSubtract(this.x, deltaX) >= 0) && sumOrSubtract(sumOrSubtract(this.x, deltaX), Constants.SPACESHIP_WIDTH) <= Constants.BASE_GAME_WIDTH) {
            this.setPosition(sumOrSubtract(this.x, deltaX), this.getY())
            for (i in 0 until this.vertices.size) {
                if(i%2 == 0) {
                    this.vertices[i] = sumOrSubtract(this.vertices[i], deltaX)
                    this.updateSpaceshipPoly()
                }
            }
        }
    }
    */

    //VERSAO COM CURRYING
    private fun moveLeftAndRight(deltaX: Float, sumOrSubtract: (Float) -> (Float) -> Float){
        if((sumOrSubtract(this.x)(deltaX) >= 0) && sumOrSubtract(sumOrSubtract(this.x)(deltaX))(Constants.SPACESHIP_WIDTH) <= Constants.BASE_GAME_WIDTH) {
            this.setPosition(sumOrSubtract(this.x)(deltaX), this.getY())
            //ATTACK ITTER
            for (i in 0 until this.vertices.size) {
                if(i%2 == 0) {
                    this.vertices[i] = sumOrSubtract(this.vertices[i])(deltaX)
                    this.updateSpaceshipPoly()
                }
            }
        }
    }

    override fun move(){
        if(this.moveRight)
            moveLeftAndRight(Constants.MOVEMENT_SPEED, Constants.curriedSum)
        if(this.moveLeft)
            moveLeftAndRight(Constants.MOVEMENT_SPEED, Constants.curriedSubtraction)
        /*val ops = Constants.curriedSubtraction(1.0f)(2.0f)
        println(ops)*/
    }

    override fun draw(game: SpaceInvadersGame, texture: Texture) {
        game.getSpriteBatch().draw(texture, this.x, this.y)
        drawShots(game)
    }

    fun createShot(){
        val x: Float = Constants.SPACESHIP_WIDTH/2 + this.x - 7.0f
        val y: Float = Constants.SPACESHIP_HEIGHT

        this.setShots(this.getShots().plus(Pair(x, y)))

        this.shotSound.play()
    }

    fun moveShots(){
        shots = shots.map{Pair(it.first, it.second + Constants.SHOT_SPEED)}
    }
    fun getShots(): List<Pair<Float, Float>> {
        return this.shots
    }
    fun setShots(newShots : List<Pair<Float, Float>>){
        this.shots = newShots
    }
    private fun drawShots(game: SpaceInvadersGame) {
        this.shots.forEach { game.getSpriteBatch().draw(Texture(Pixmap(Gdx.files.internal(Constants.SHOTS_TEXTURE))), it.first, it.second) }
    }
    fun removeOutterShots(){

        this.shots = shots.filter(fun(bullet) : Boolean = bullet.second <= Constants.BASE_GAME_HEIGHT)

    }
}