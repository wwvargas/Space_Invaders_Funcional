package com.mygdx.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Texture
import com.mygdx.values.Constants
import com.mygdx.screens.GameRunning
import com.badlogic.gdx.math.Intersector
import com.badlogic.gdx.math.Polygon

class EnemyHorde(private val playerSpaceship: PlayerSpaceship, private val gameRunning: GameRunning) {

    private var enemyHorde = mutableListOf< EnemySpaceship >()
    private var difficulty: Int = 0
    private var moveLeft: Boolean = false
    private var moveRight: Boolean = true
    private var changeLane: Boolean = false
    private var explosionSound: Sound = Gdx.audio.newSound(Gdx.files.internal(Constants.EXPLOSION_SOUND))


    private fun createEnemies(line : Int, col : Int)
    {
        val x: Float
        val y: Float
        val health: Int

        if (col >=0){
            x = Constants.HORDE_X + (col * Constants.ENEMY_WIDTH) + (col * Constants.SPACE_BETWEEN_COLUMNS_OF_ENEMIES)
            y = Constants.HORDE_Y + (line * Constants.ENEMY_HEIGHT)
            health = this.difficulty
            this.enemyHorde.add(EnemySpaceship(x, y, health))
            createEnemies(line, col - 1)
        }
    }

    private fun createLines(line : Int){
        if (line >= 0) {
            createEnemies(line, 4)
            createLines(line - 1)
        }
    }

    fun createHordeIfNeeded(){
        if(this.isEmpty()) {
            if (this.difficulty<3)
                this.difficulty += 1 // every new horde increments the difficulty until it reaches 3

            createLines(this.difficulty + 1)

        }
    }

    private fun isEmpty(): Boolean{
        return enemyHorde.size == 0
    }

    fun getEnemyHorde(): MutableList< EnemySpaceship >{
        return this.enemyHorde
    }

    private fun moveEnemy(enemy: Int){
        if(enemy < enemyHorde.size) {
            if (this.moveRight && this.enemyHorde[enemy].x + Constants.ENEMY_WIDTH + (2.0f * this.difficulty) > Constants.BASE_GAME_WIDTH - 5.0f) {
                this.changeLane = true
                this.moveLeft = true
                this.moveRight = false
            } else if (this.moveLeft && this.enemyHorde[enemy].x - (2.0f * this.difficulty) < 5.0f) {
                this.changeLane = true
                this.moveLeft = false
                this.moveRight = true
            } else {
                moveEnemy(enemy + 1)
            }
        }
    }
    private fun configEnemies(enemy : Int)
    {
        if (enemy < enemyHorde.size) {
            enemyHorde[enemy].setChangeLane(this.changeLane)
            enemyHorde[enemy].setSpeed(2.0f * this.difficulty)
            enemyHorde[enemy].publicSetMoveRight(this.moveRight)
            enemyHorde[enemy].publicSetMoveLeft(this.moveLeft)
            this.enemyHorde[enemy].move()
            configEnemies(enemy + 1)
        }
    }
    fun moveHorde(){
        this.changeLane = false
        moveEnemy(0)
        configEnemies(0)
    }

    //collision of a shot with an enemy
    private fun collision(enemy: EnemySpaceship, shootx: Float, shooty: Float) : Boolean{

        val shootPoly = Polygon()
        val verticesForShoot = floatArrayOf(
                shootx, shooty,
                shootx + Constants.SHOT_WIDTH, shooty,
                shootx, shooty + Constants.SHOT_HEIGHT,
                shootx + Constants.SHOT_WIDTH, shooty + Constants.SHOT_HEIGHT)
        shootPoly.vertices = verticesForShoot
        return (Intersector.overlapConvexPolygons(enemy.getSpaceshipPoly(), shootPoly))

    }

    //collision of an enemy with the player
    private fun collision(enemy : EnemySpaceship, player : PlayerSpaceship) : Boolean{
        return (Intersector.overlapConvexPolygons(enemy.getSpaceshipPoly(), player.getSpaceshipPoly()))
    }

    fun checkCollision(){
        val shotIterator = this.playerSpaceship.getShots().iterator()
        var collision : Boolean
        for (shot in shotIterator){
            val enemyIterator = this.enemyHorde.iterator()
            collision = false
            while (enemyIterator.hasNext() && !collision){
                val enemy = enemyIterator.next()
                if(collision(enemy, shot.first, shot.second))
                {
                    collision = true
                    enemy.publicSetHealth(enemy.publicGetHealth()-1)
                    if(enemy.publicGetHealth()==0) {
                        enemyIterator.remove()
                        gameRunning.setCurrentScore(gameRunning.getCurrentScore() + 50)
                    }
                    else
                        enemy.takeDamage()

                    this.explosionSound.play()
                    this.playerSpaceship.setShots(this.playerSpaceship.getShots().minus(shot))
                }
            }
        }
        this.enemyHorde.iterator().forEach {
            if(collision(it, this.playerSpaceship))
            {
                this.playerSpaceship.publicSetHealth(this.playerSpaceship.publicGetHealth()-1)
            }
        }

    }

    fun checkEndOfGame(): Boolean {
        //ATTACK ITTER
        val enemyIterator = this.enemyHorde.iterator()
        this.enemyHorde.iterator().forEach {
            if (it.y <= 0)
                return true
            if(this.playerSpaceship.publicGetHealth()<1)
            {
                return true
            }
        }
        return false
    }

    private fun drawEnemy(game: SpaceInvadersGame, texture: Texture, ind : Int)
    {
        if (ind < enemyHorde.size) {
            val enemy = this.enemyHorde[ind]
            if (!enemy.wasHit() || enemy.getRecoverTime() % 10 < 5) {
                this.enemyHorde[ind].draw(game, texture)
            }
            enemy.decreaseRecoverTime()
            drawEnemy(game, texture, ind + 1)
        }
    }

    fun draw(game: SpaceInvadersGame, texture: Texture){
        drawEnemy(game, texture, 0)
    }

}